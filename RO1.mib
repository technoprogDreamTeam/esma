TECHNOPROG-MIB DEFINITIONS ::= BEGIN

IMPORTS
	enterprises, mgmt, TimeTicks
		FROM RFC1155-SMI
	OBJECT-TYPE
		FROM RFC-1212
	TRAP-TYPE
		FROM RFC-1215;


TC1 ::= INTEGER


-- MIB-II
mib-2                OBJECT IDENTIFIER ::= { mgmt 1 }
system               OBJECT IDENTIFIER ::= { mib-2 1 }

-- enterprises

llcTECHNOPROG               OBJECT IDENTIFIER ::= { enterprises 53441 }

-- branch for MALS radio equipment

malsREq		     OBJECT IDENTIFIER ::= { llcTECHNOPROG 1 }
states		     OBJECT IDENTIFIER ::= { malsREq 1 }


-- system ---------------------------------------------------------------------

sysDescr  OBJECT-TYPE
	SYNTAX     OCTET STRING (SIZE(0..255))
	ACCESS     read-only
	STATUS     mandatory
	DESCRIPTION 
		"A textual description of the entity."
	::= { system 1 }

sysObjectID  OBJECT-TYPE
	SYNTAX     OBJECT IDENTIFIER
	ACCESS     read-only
	STATUS     mandatory
	DESCRIPTION 
		"The vendor's authoritative identification of the
		                network management subsystem contained in the
		                entity."
	::= { system 2 }

sysUpTime  OBJECT-TYPE
	SYNTAX     TimeTicks
	ACCESS     read-only
	STATUS     mandatory
	DESCRIPTION 
		"The time (in hundredths of a second) since the
		                network management portion of the system was last
		                re-initialized."
	::= { system 3 }

powerStates  OBJECT-TYPE
	SYNTAX     INTEGER
	ACCESS     read-only
	STATUS     mandatory
	DESCRIPTION 
		"
	States of the various elements of the power subsystem:
		0 bit - state of relay KV1 (0 - OFF, 1 - ON)
		1 bit - state of relay KV2 (0 - OFF, 1 - ON)
		2 bit - state of relay KV3 (0 - OFF, 1 - ON)
		3 bit - state of automate QF1 (0 - OFF, 1 - ON)
		4 bit - state of automate QF2 (0 - OFF, 1 - ON)
		5 bit - state of automate QF3 (0 - OFF, 1 - ON)
		6 bit - state of automate QF4 (0 - OFF, 1 - ON)
		7 bit - state of automate QF5 (0 - OFF, 1 - ON)
		8 bit - state of automate QF6 (0 - OFF, 1 - ON)
		9 bit - state of automate QF7 (0 - OFF, 1 - ON)
		10 bit - state of automate QF8 (0 - OFF, 1 - ON)
		11,12 bits - state of switch QS (00 - Neutral; 01 - I ON, II OFF; 10 - I OFF, II ON, 11 - Reserve)		
		13 bit - state of power supply unit PS1 (0 - Output Fault, 1 - Output OK)
		14 bit - state of power supply unit PS2 (0 - Output Fault, 1 - Output OK)	
		15 bit - state of power supply unit PS3 (0 - Output Fault, 1 - Output OK)		
		16 bit - state of power supply unit PS4 (0 - Output Fault, 1 - Output OK)
		17 bit - state of power supply unit PS5 (0 - Output Fault, 1 - Output OK)	
		18 bit - state of power supply unit PS6 (0 - Output Fault, 1 - Output OK)
		...
		20,21,22 bits - importance of event (000 - clear, 001 - minor, 010 - major, 011 - critical, 100 - warning, 101..111 - Reserve)			
		23 bit - type of event (0 - accident ending, 1 - accident start)			
		"
	::= { states 1 }


cabinetStates  OBJECT-TYPE
	SYNTAX     INTEGER
	ACCESS     read-only
	STATUS     mandatory
	DESCRIPTION 
		"
	States of the various elements of the radio equipment cabinet:
		0 bit - state of the fan unit (0 - Fault, 1 - OK)
		1 bit - state of the front door (0 - Opened, 1 - Closed)
		2 bit - state of the back door (0 - Opened, 1 - Closed)
		...
		20,21,22 bits - importance of event (000 - clear, 001 - minor, 010 - major, 011 - critical, 100 - warning, 101..111 - Reserve)			
		23 bit - type of event (0 - accident ending, 1 - accident start)			
		"
	::= { states 2 }


ups1State  OBJECT-TYPE
	SYNTAX     INTEGER
	ACCESS     read-only
	STATUS     mandatory
	DESCRIPTION 
		"
		state of UPS1
		0 bit - state of UPS1 (0 - Fault, 1 - OK)
		...
		20,21,22 bits - importance of event (000 - clear, 001 - minor, 010 - major, 011 - critical, 100 - warning, 101..111 - Reserve)			
		23 bit - type of event (0 - accident ending, 1 - accident start)				
		"
	::= { states 3 }

ups2State  OBJECT-TYPE
	SYNTAX     INTEGER
	ACCESS     read-only
	STATUS     mandatory
	DESCRIPTION 
		"
		state of UPS2
		0 bit - state of UPS2 (0 - Fault, 1 - OK)
		...
		20,21,22 bits - importance of event (000 - clear, 001 - minor, 010 - major, 011 - critical, 100 - warning, 101..111 - Reserve)			
		23 bit - type of event (0 - accident ending, 1 - accident start)			
		"
	::= { states 4 }

ic1State  OBJECT-TYPE
	SYNTAX     INTEGER
	ACCESS     read-only
	STATUS     mandatory
	DESCRIPTION 
		"
		State of the interface convertor IC1
		0 bit - state of IC1 (0 - Fault, 1 - OK)
		...
		20,21,22 bits - importance of event (000 - clear, 001 - minor, 010 - major, 011 - critical, 100 - warning, 101..111 - Reserve)			
		23 bit - type of event (0 - accident ending, 1 - accident start)		
		"
	::= { states 5 }

ic2State  OBJECT-TYPE
	SYNTAX     INTEGER
	ACCESS     read-only
	STATUS     mandatory
	DESCRIPTION 
		"
		State of the interface convertor IC2
		0 bit - state of IC2 (0 - Fault, 1 - OK)
		...
		20,21,22 bits - importance of event (000 - clear, 001 - minor, 010 - major, 011 - critical, 100 - warning, 101..111 - Reserve)			
		23 bit - type of event (0 - accident ending, 1 - accident start)	
		"
	::= { states 6 }

rm1State  OBJECT-TYPE
	SYNTAX     INTEGER
	ACCESS     read-only
	STATUS     mandatory
	DESCRIPTION 
		"
		State of the radio modem RM1
		0 bit - state of RM1 (0 - Fault, 1 - OK)
		...
		20,21,22 bits - importance of event (000 - clear, 001 - minor, 010 - major, 011 - critical, 100 - warning, 101..111 - Reserve)			
		23 bit - type of event (0 - accident ending, 1 - accident start)				
		"
	::= { states 7 }


rm2State  OBJECT-TYPE
	SYNTAX     INTEGER
	ACCESS     read-only
	STATUS     mandatory
	DESCRIPTION 
		"
		State of the radio modem RM2
		0 bit - state of RM2 (0 - Fault, 1 - OK)
		...
		20,21,22 bits - importance of event (000 - clear, 001 - minor, 010 - major, 011 - critical, 100 - warning, 101..111 - Reserve)			
		23 bit - type of event (0 - accident ending, 1 - accident start)	
		"
	::= { states 8 }

gnssr1State  OBJECT-TYPE
	SYNTAX     INTEGER
	ACCESS     read-only
	STATUS     mandatory
	DESCRIPTION 
		"
		State of the global navigation satellite system receiver GNSSR1 
		0 bit - state of GNSSR1 (0 - Fault, 1 - OK)
		...
		20,21,22 bits - importance of event (000 - clear, 001 - minor, 010 - major, 011 - critical, 100 - warning, 101..111 - Reserve)			
		23 bit - type of event (0 - accident ending, 1 - accident start)		
		"
	::= { states 9 }

gnssr2State  OBJECT-TYPE
	SYNTAX     INTEGER
	ACCESS     read-only
	STATUS     mandatory
	DESCRIPTION 
		"
		State of the global navigation satellite system receiver GNSSR2 
		0 bit - state of GNSSR2 (0 - Fault, 1 - OK)
		...
		20,21,22 bits - importance of event (000 - clear, 001 - minor, 010 - major, 011 - critical, 100 - warning, 101..111 - Reserve)			
		23 bit - type of event (0 - accident ending, 1 - accident start)	
		"
	::= { states 10 }

sw1State  OBJECT-TYPE
	SYNTAX     INTEGER
	ACCESS     read-only
	STATUS     mandatory
	DESCRIPTION 
		"
		State of the network switch SW1	
		0 bit - state of SW1 (0 - Fault, 1 - OK)
		...
		20,21,22 bits - importance of event (000 - clear, 001 - minor, 010 - major, 011 - critical, 100 - warning, 101..111 - Reserve)			
		23 bit - type of event (0 - accident ending, 1 - accident start)	
		"
	::= { states 11 }

sw2State  OBJECT-TYPE
	SYNTAX     INTEGER
	ACCESS     read-only
	STATUS     mandatory
	DESCRIPTION 
		"
		State of the network switch SW2
		0 bit - state of SW2 (0 - Fault, 1 - OK)
		...
		20,21,22 bits - importance of event (000 - clear, 001 - minor, 010 - major, 011 - critical, 100 - warning, 101..111 - Reserve)			
		23 bit - type of event (0 - accident ending, 1 - accident start)	
		"
	::= { states 12 }





statesChanged  TRAP-TYPE
	ENTERPRISE malsREq
	DESCRIPTION 
		"
		"
	::= 1



END




