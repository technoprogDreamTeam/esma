#!/bin/bash

. ./esma.cfg
. ./esma-defs
. ./discrete.sh
. ./moxa/moxa.sh

STATE=0
PREV_STATE=0
ST=0
PST=0
MSG=0

StatesArr=(\
	"power" \
	"cabinet" \
	"ups1" \
	"ups2" \
	"ic1" \
	"ic2" \
	"rm1" \
	"rm2" \
	"gnssr1" \
	"gnssr2" \
	"sw1" \
	"sw2" \
)

OIDarr=(\
	$POWER_OID \
	$CABINET_OID \
	$UPS1_OID \
	$UPS2_OID \
	$IC1_OID \
	$IC2_OID \
	$RM1_OID \
	$RM2_OID \
	$GNSSR1_OID \
	$GNSSR2_OID \
	$SW1_OID \
	$SW2_OID \
)

ImportanceArr=(\
	$IMP_FLAG_WARNING \
	$IMP_FLAG_WARNING \
	$IMP_FLAG_WARNING \
	$IMP_FLAG_WARNING \
	$IMP_FLAG_WARNING \
	$IMP_FLAG_WARNING \
	$IMP_FLAG_WARNING \
	$IMP_FLAG_WARNING \
	$IMP_FLAG_WARNING \
	$IMP_FLAG_WARNING \
	$IMP_FLAG_WARNING \
	$IMP_FLAG_WARNING \
)

ImportanceCabinetArr=(\
        $IMP_FLAG_WARNING \
        $IMP_FLAG_WARNING \
        $IMP_FLAG_WARNING \
)

ImportancePowerBeforeQSArr=(\
        $IMP_FLAG_WARNING \
        $IMP_FLAG_WARNING \
        $IMP_FLAG_WARNING \
        $IMP_FLAG_WARNING \
        $IMP_FLAG_WARNING \
        $IMP_FLAG_WARNING \
        $IMP_FLAG_WARNING \
        $IMP_FLAG_WARNING \
        $IMP_FLAG_WARNING \
        $IMP_FLAG_WARNING \
        $IMP_FLAG_WARNING \
)

ImportancePowerQS=$IMP_FLAG_WARNING

ImportancePowerAfterQSArr=(\
        $IMP_FLAG_WARNING \
        $IMP_FLAG_WARNING \
        $IMP_FLAG_WARNING \
        $IMP_FLAG_WARNING \
        $IMP_FLAG_WARNING \
        $IMP_FLAG_WARNING \
)



NetDevicesArr=(\
	"ups1"\
	"ups2"\
	"ic1" \
	"ic2" \
	"sw1" \
	"sw2" \
)

NetDevicesIParr=(\
	$UPS1_IP \
	$UPS2_IP \
	$IC1_IP  \
	$IC2_IP  \
	$SW1_IP  \
	$SW2_IP  \
)


function esma_get_state {

	read tmp < $STATES_DIR/$1
	STATE=$(($tmp))

}


function esma_get_power_state {

        read tmp < $POWERSTATES_DIR/$1
        ST=$(($tmp))
}


function esma_get_cabinet_state {

        read tmp < $CABINETSTATES_DIR/$1
        ST=$(($tmp))
}



function esma_prepare_msg {
	MSG=$(($STATE + $1 + $2))
	#echo $MSG
}


function esma_get_prev_state {

	read tmp < $PREVSTATES_DIR/$1
	PREV_STATE=$(($tmp))
}


function esma_get_cabinet_prev_state {

        read tmp < $CABINETPREVSTATES_DIR/$1
        PST=$(($tmp))
}

function esma_get_power_prev_state {

        read tmp < $POWERPREVSTATES_DIR/$1
        PST=$(($tmp))
}



function esma_set_prev_state {

	cp -r $STATES_DIR/* $PREVSTATES_DIR/

}

function esma_check_net_device {
	ping -c 2 -i 0.2 $2 > /dev/null
	if [ $? -eq 0 ]
	then
		echo "*** device "$1 "ok."
		echo "1" >$STATES_DIR/$1
	else
		echo "*** device "$1 "fault."
		echo "0" >$STATES_DIR/$1
	fi
}



echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
echo "ESMA script Start"


#date  >>/run/shm/log


#MSG=$((16#FFFFF0))
#./trapsend.sh $HOST_IP ${OIDarr[0]} $MSG

if [ -d $STATES_DIR ]
then
	echo -en '\n'
else
	echo -en '\n'
	mkdir $STATES_DIR/
	cp -r ./states/* $STATES_DIR/
fi

if [ -d $PREVSTATES_DIR ]
then
	echo -en '\n'
else
	echo -en '\n'
	mkdir $PREVSTATES_DIR
	cp -r ./states/* $PREVSTATES_DIR/
fi

echo "1.1. Check discrete signals"
esma_discrete_in $(./discrete/discrete)
esma_split_discrete


for i in ${!NetDevicesArr[@]}; do

	echo "2."$(($i+1))". Check device "${NetDevicesArr[$i]}" with IP "${NetDevicesIParr[$i]}" on net."
	esma_check_net_device ${NetDevicesArr[$i]} ${NetDevicesIParr[$i]}

done

echo -en '\n'

#TMP=$((2#1111110011111111111))
#MSG=$(($((2#10))<<11))
#MSG=$(($MSG+$TMP))
#echo $MSG
#./trapsend.sh $HOST_IP $POWER_OID $MSG

esma_get_power_state qs

if [ $ST -eq $QSFIRST_MASK ]
then
	./moxa/moxa_telnet $MOXA1_IP $MOXA1_PASSWORD 2>/dev/null 1>$TRAFFICSTATES_DIR/TMP
        if [ $? -eq 0 ]
        then
                mv -f $TRAFFICSTATES_DIR/TMP $MOXA1_TELNET_FNAME
		echo "3.1. Check rm1."
		esma_moxa_traffic $MOXA1_TELNET_FNAME $RM1_MOXA_PORT $TRAFFICSTATES_DIR/rm1
		echo "3.2. Check gnssr1."
		esma_moxa_traffic $MOXA1_TELNET_FNAME $GNSSR1_MOXA_PORT $TRAFFICSTATES_DIR/gnssr1
	else
                echo "3.1. Check Moxa1: Error"
                rm $TRAFFICSTATES_DIR/TMP
	fi
else
	./moxa/moxa_telnet $MOXA2_IP $MOXA2_PASSWORD 2>/dev/null 1>$TRAFFICSTATES_DIR/TMP 
	if [ $? -eq 0 ]
	then
		mv -f $TRAFFICSTATES_DIR/TMP $MOXA2_TELNET_FNAME
		echo "3.1. Check rm2."
		esma_moxa_traffic $MOXA2_TELNET_FNAME $RM2_MOXA_PORT $TRAFFICSTATES_DIR/rm2
		echo "3.2. Check gnssr2."
		esma_moxa_traffic $MOXA2_TELNET_FNAME $GNSSR2_MOXA_PORT $TRAFFICSTATES_DIR/gnssr2
	else
		rm $TRAFFICSTATES_DIR/TMP
		echo "3.1. Check Moxa2: Error"
	fi
fi


echo -en '\n'


for i in ${!StatesArr[@]}; do

	echo "4."$(($i+1))". Check and report state of" ${StatesArr[$i]}"."
	esma_get_state ${StatesArr[$i]}
	esma_get_prev_state ${StatesArr[$i]}

	if [ $STATE -ne $PREV_STATE ]
	then
        	echo "+++ State of "${StatesArr[$i]}" is changed. We send trap(s) to client."

		if [ ${StatesArr[$i]} == "power" ]
		then
                        for j in ${!PowerStatesBeforeQSArr[@]}; do
                                esma_get_power_state ${PowerStatesBeforeQSArr[$j]}
                                esma_get_power_prev_state ${PowerStatesBeforeQSArr[$j]}
                                if [ $ST -ne $PST ]
                                then
                                        echo ">>>> State of "${PowerStatesBeforeQSArr[$j]}" is changed. We send trap to client."
                                        if [ $ST -lt $PST ]
                                        then
                                                esma_prepare_msg $TYPE_FLAG_START ${ImportancePowerBeforeQSArr[$j]}
                                        else
                                                esma_prepare_msg $TYPE_FLAG_STOP $IMP_FLAG_CLEAR

                                        fi
                                        ./trapsend.sh $HOST_IP ${OIDarr[$i]} $MSG
                                fi
                        done

                        esma_get_power_state qs
                        esma_get_power_prev_state qs
                        if [ $ST -ne $PST ]
                        then
                           echo ">>>> State of qs is changed. We send trap to client."
                           esma_prepare_msg $TYPE_FLAG_START $ImportancePowerQS
                           ./trapsend.sh $HOST_IP ${OIDarr[$i]} $MSG
                        fi

                        for j in ${!PowerStatesAfterQSArr[@]}; do
                                esma_get_power_state ${PowerStatesAfterQSArr[$j]}
                                esma_get_power_prev_state ${PowerStatesAfterQSArr[$j]}
                                if [ $ST -ne $PST ]
                                then
                                        echo ">>>> State of "${PowerStatesAfterQSArr[$j]}" is changed. We send trap to client."
                                        if [ $ST -lt $PST ]
                                        then
                                                esma_prepare_msg $TYPE_FLAG_START ${ImportancePowerAfterQSArr[$j]}
                                        else
                                                esma_prepare_msg $TYPE_FLAG_STOP $IMP_FLAG_CLEAR

                                        fi
                                        ./trapsend.sh $HOST_IP ${OIDarr[$i]} $MSG
                                fi
                        done

		elif [ ${StatesArr[$i]} == "cabinet" ]
		then
		        for j in ${!CabinetStatesArr[@]}; do
				esma_get_cabinet_state ${CabinetStatesArr[$j]}
				esma_get_cabinet_prev_state ${CabinetStatesArr[$j]}
                		if [ $ST -ne $PST ]
				then
			        	echo ">>>> State of "${CabinetStatesArr[$j]}" is changed. We send trap to client."
		                        if [ $ST -lt $PST ]
                		        then
                                		esma_prepare_msg $TYPE_FLAG_START ${ImportanceCabinetArr[$j]}
                        		else
                                		esma_prepare_msg $TYPE_FLAG_STOP $IMP_FLAG_CLEAR

                        		fi
					./trapsend.sh $HOST_IP ${OIDarr[$i]} $MSG
				fi
        		done

		else
	        	if [ $STATE -lt $PREV_STATE ]
        		then
				esma_prepare_msg $TYPE_FLAG_START ${ImportanceArr[$i]}
			else
				esma_prepare_msg $TYPE_FLAG_STOP $IMP_FLAG_CLEAR

			fi
			./trapsend.sh $HOST_IP ${OIDarr[$i]} $MSG

		fi
	else
        	echo "+++ State of "${StatesArr[$i]}" is not changed."
	fi


#	echo -en '\n'

done

esma_set_prev_state 



echo -en '\n'
echo -en '\n'
echo "ESMA script End"
echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"

