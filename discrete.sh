#!/bin/bash

. ./esma-discrete-defs
. ./esma-defs

PowerStatesBeforeQSArr=(\
        "kv1" \
        "kv2" \
        "kv3" \
        "qf1" \
        "qf2" \
        "qf3" \
        "qf4" \
        "qf5" \
        "qf6" \
        "qf7" \
        "qf8" \
)

PowerStatesAfterQSArr=(\
        "ps1" \
        "ps2" \
        "ps3" \
        "ps4" \
        "ps5" \
        "ps6" \
)

CabinetStatesArr=(\
        "fan" \
        "frontdoor" \
        "backdoor" \
)




function esma_discrete_in {

	PA=$1
	PB=$2
	PC=$3
#echo "PA="$PA
#echo "PB="$PB
#echo "PC="$PC

#PA=2#00000111
#PB=2#00000000
#PC=2#10000000

	POWERSTATES=0
	CABINETSTATES=0

	#tests for power states

        #test KV1 state
        if [ $(($PA & KV1_CPC150_MASK)) == 0 ]
        then
                POWERSTATES=$(($POWERSTATES | $KV1_POWERSTATES_MASK))
        fi

        #test KV2 state
        if [ $(($PA & KV2_CPC150_MASK)) == 0 ]
        then
                POWERSTATES=$(($POWERSTATES | $KV2_POWERSTATES_MASK))
        fi

        #test KV3 state
        if [ $(($PB & KV3_CPC150_MASK)) == 0 ]
        then
                POWERSTATES=$(($POWERSTATES | $KV3_POWERSTATES_MASK))
        fi

        #test QF1 state
        if [ $(($PA & QF1_CPC150_MASK)) != 0 ]
        then
                POWERSTATES=$(($POWERSTATES | $QF1_POWERSTATES_MASK))
        fi

        #test QF2 state
        if [ $(($PA & QF2_CPC150_MASK)) != 0 ]
        then
                POWERSTATES=$(($POWERSTATES | $QF2_POWERSTATES_MASK))
        fi

        #test QF3 state
        if [ $(($PA & QF3_CPC150_MASK)) != 0 ]
        then
                POWERSTATES=$(($POWERSTATES | $QF3_POWERSTATES_MASK))
        fi

        #test QF4 state
        if [ $(($PA & QF4_CPC150_MASK)) != 0 ]
        then
                POWERSTATES=$(($POWERSTATES | $QF4_POWERSTATES_MASK))
        fi

        #test QF5 state
        if [ $(($PC & QF5_CPC150_MASK)) != 0 ]
        then
                POWERSTATES=$(($POWERSTATES | $QF5_POWERSTATES_MASK))
        fi

        #test QF6 state
        if [ $(($PC & QF6_CPC150_MASK)) != 0 ]
        then
                POWERSTATES=$(($POWERSTATES | $QF6_POWERSTATES_MASK))
        fi

        #test QF7 state
        if [ $(($PC & QF7_CPC150_MASK)) != 0 ]
        then
                POWERSTATES=$(($POWERSTATES | $QF7_POWERSTATES_MASK))
        fi

        #test QF8 state
        if [ $(($PC & QF8_CPC150_MASK)) != 0 ]
        then
                POWERSTATES=$(($POWERSTATES | $QF8_POWERSTATES_MASK))
        fi

	#test QS state
        if [ $(($PC & QS1_CPC150_MASK)) == 0 ]
	then
		if [ $(($PB & QS2_CPC150_MASK)) == 0 ]
		then
			POWERSTATES=$(($POWERSTATES | $QSRESERVE_POWERSTATES_MASK))
		else
			POWERSTATES=$(($POWERSTATES | $QSFIRST_POWERSTATES_MASK))
		fi
	else
                if [ $(($PB & QS2_CPC150_MASK)) == 0 ]
                then
                        POWERSTATES=$(($POWERSTATES | $QSSECOND_POWERSTATES_MASK))
                else
                        POWERSTATES=$(($POWERSTATES | $QSNEUTRAL_POWERSTATES_MASK))
                fi
	fi




        #test PS1 state
        if [ $(($PC & PS1_CPC150_MASK)) == 0 ]
        then
                POWERSTATES=$(($POWERSTATES | $PS1_POWERSTATES_MASK))
        fi

        #test PS2 state
        if [ $(($PC & PS2_CPC150_MASK)) == 0 ]
        then
                POWERSTATES=$(($POWERSTATES | $PS2_POWERSTATES_MASK))
        fi

        #test PS3 state
        if [ $(($PC & PS3_CPC150_MASK)) == 0 ]
        then
                POWERSTATES=$(($POWERSTATES | $PS3_POWERSTATES_MASK))
        fi

        #test PS4 state
        if [ $(($PB & PS4_CPC150_MASK)) == 0 ]
        then
                POWERSTATES=$(($POWERSTATES | $PS4_POWERSTATES_MASK))
        fi

        #test PS5 state
        if [ $(($PB & PS5_CPC150_MASK)) == 0 ]
        then
                POWERSTATES=$(($POWERSTATES | $PS5_POWERSTATES_MASK))
        fi

        #test PS6 state
        if [ $(($PB & PS6_CPC150_MASK)) == 0 ]
        then
                POWERSTATES=$(($POWERSTATES | $PS6_POWERSTATES_MASK))
        fi


	#tests for cabinet states

	#test front door state
	if [ $(($PA & FRONTDOOR_CPC150_MASK)) == 0 ]
	then
		CABINETSTATES=$(($CABINETSTATES | $FRONTDOOR_CABINETSTATES_MASK))
	fi

	#test back door state
        if [ $(($PA & BACKDOOR_CPC150_MASK)) == 0 ]
        then
                CABINETSTATES=$(($CABINETSTATES | $BACKDOOR_CABINETSTATES_MASK))
        fi

        #test fan state
        if [ $(($PB & FAN12_CPC150_MASK)) == 0 ] && [ $(($PB & FAN34_CPC150_MASK)) == 0 ] && [ $(($PB & KV4_CPC150_MASK)) == 0 ]
        then
                CABINETSTATES=$(($CABINETSTATES | $FAN_CABINETSTATES_MASK))
        fi

	echo "*** POWERSTATES="$POWERSTATES
	echo "*** CABINETSTATES="$CABINETSTATES

	echo $POWERSTATES > $STATES_DIR/power
	echo $CABINETSTATES > $STATES_DIR/cabinet
}



function esma_split_discrete {

	TMP=$POWERSTATES

	for i in ${!PowerStatesBeforeQSArr[@]}; do
		echo $(($TMP & 2#1)) > $POWERSTATES_DIR/${PowerStatesBeforeQSArr[$i]}
		TMP=$(($TMP >> 1))
	done

	echo $(($TMP & 2#11)) > $POWERSTATES_DIR/qs
	TMP=$(($TMP >> 2))

        for i in ${!PowerStatesAfterQSArr[@]}; do
                echo $(($TMP & 2#1)) > $POWERSTATES_DIR/${PowerStatesAfterQSArr[$i]}
                TMP=$(($TMP >> 1))
        done

	TMP=$CABINETSTATES

        for i in ${!CabinetStatesArr[@]}; do
                echo $(($TMP & 2#1)) > $CABINETSTATES_DIR/${CabinetStatesArr[$i]}
                TMP=$(($TMP >> 1))
        done

}


#esma_discrete_in $(./discrete/discrete)
#esma_split_discrete
