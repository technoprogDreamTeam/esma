# This script parses traffic data
# TxCnt, RxCnt, TxTotalCnt, RxTotalCnt, DSR, CTS, DCD
# for different COM ports from moxa telnet session
# function esma_moxa_traffic 
# prints data for port number $2 to file with name $3
# %1 - name of file with moxa telnet session data
# each value to one string:
# TxCnt
# RxCnt
# TxTotalCnt
# RxTotalCnt
# DSR
# CTS
# DCD


#!/bin/bash
#. ../esma-defs

function esma_moxa_traffic {

# Del traffic data file name, if it exists 
	if [ -e $3 ]
	then
		rm $3
	fi

# Read data from moxa telnet session to string array
# Our String likes that (very long string with some escape sequences):
# Press 'q' to cancel ... [4;7H                                           [4;7H0[4;18H0[4;29H0[4;40H0[4;51HOFF[4;55HOFF[4;59HOFF[5;7H                                           [5;7H0[5;18H0[5;29H0[5;40H0[5;51HOFF[5;55HOFF[5;59HOFF[6;7H 
#  "Press 'q' to cancel ..."  - begin  of string with traffic data
	arr=($(cat $1 | grep -a "Press 'q' to cancel ..."))

# "Press" "'q'" "to" "cancel" "..." - skip 5 strings
# each port corresponds string like "[4;7H0[4;18H0[4;29H0[4;40H0[4;51HOFF[4;55HOFF[4;59HOFF[5;7H"
	str=${arr[$(($2+5))]}


# for each port we have seven values: TxCnt, RxCnt, TxTotalCnt, RxTotalCnt, DSR, CTS, DCD
#                                   "[4;7H0[4;18H0[4;29H0[4;40H0[4;51HOFF[4;55HOFF[4;59HOFF[5;7H"
#                                         |1     |2     |3     |4     |5       |6       |7

	for (( i=1; i <= 7; i++ ))
	do
#fix each value between symbols "H" and ";" in string
		indx=$(expr index $str 'H')
		str=${str:$indx}
		indx1=$(expr index $str ";")
#all lines end with ";7H" except very last value (DCD for 4th port)
		if [ $2 -eq "4" ] && [ $i -eq "7" ]
		then
			str1=$str
		else
			str1=${str:0:$(($indx1-4))}
		fi
#print value
#		echo $str1
		echo $str1 >> $3
	done

}






