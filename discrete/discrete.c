/*
 * example.c: very simple example of port I/O
 *
 * This code does nothing useful, just a port write, a pause,
 * and a port read. Compile with `gcc -O2 -o discrete discrete.c',
 * and run as root with `./example'.
 */

#include <stdio.h>
#include <unistd.h>
#include <sys/io.h>

#define BASEPORT 0x310 


int main()
{

  char sbuf[20];
  unsigned char PA, PB, PC;
  int BAD;
  unsigned char a310, a311;
//  int cnt = 0;

  /* Get access to the ports */
  if (ioperm(BASEPORT, 2, 1)) {perror("ioperm"); exit(1);}

  /* Get Base Address for CPC150 onboard discrete IO */
  a310 = inb(BASEPORT);
  a311 = inb(BASEPORT+1);
  BAD = ((int)a310) << 4;
  BAD += ((int)(a311 & 0xf)) << 12;
//  printf("port 310 = 0x%x       port 311 = 0x%x  BAD = 0x%x\n" ,a310 , a311, BAD);

  /* We don't need the ports anymore */
  if (ioperm(BASEPORT, 2, 0)) {perror("ioperm"); exit(1);}


  /* Get access to the ports */
  if (iopl(3)) {perror("ioperm"); exit(1);}
  if (ioperm(BAD, 15, 1)) {perror("ioperm"); exit(1);}

  /* Configure Directions of Ports: all of 24 are inputs */
  outb(BAD+3, 0x9B);

//  while (1) { 
    PA = inb( BAD + 0);
    PB = inb( BAD + 1);
    PC = inb( BAD + 2);

    printf("0x%x 0x%x 0x%x\n", PA, PB, PC);

/*
    byte2binarystr( PA, sbuf);
    printf("PA = %s\n", sbuf);

    byte2binarystr( PB, sbuf);
    printf("PB = %s\n", sbuf);

    byte2binarystr( PC, sbuf);
    printf("PC = %s\n\n", sbuf);
*/

    /* Sleep for a while (1000 ms) */
//    usleep(1000000);

//  }

  /* We don't need the ports anymore */
  if (ioperm(BAD, 15, 0)) {perror("ioperm"); exit(1);}
  if (iopl(0)) {perror("ioperm"); exit(1);}



  exit(0);


}
