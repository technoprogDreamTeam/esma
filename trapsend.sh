#!/bin/bash


. ./esma-defs

HOST=$1
OID=$2

if [ -n "$3" ]
then
VALUE=$3
else
VALUE=$DEFAULT_VALUE
fi

sudo snmptrap -M $MIBDIR -mAll -v1 -c public $HOST  $TRAPNAME "" 6 17 "" $OID  i $VALUE

